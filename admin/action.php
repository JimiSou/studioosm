<?php
$time_start = microtime(true);
include ('safe.php');
include ('access.php');

function getAction($id)
{
	$dotaz="SELECT  * FROM  akce WHERE `id`='".$id."'; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
	$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 	
 		if ($vysledek)
			{
				
				return $vysledek->fetch_assoc(); //vr·cenÌ pole(celÈho z·znamu)
				
			}
			else 
			{
				echo "Nastala chyba! Zkuste to prosím znovu.";
			}
 	
}

function listAction()
{
	$dotaz="SELECT  * FROM  akce ORDER BY `datum` DESC; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
	$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 	if ($vysledek) 
 	{
    	echo '<h3>Akce</h3><table align="center"><th>Datum konání</th><th>Nadpis</th>';
    	
    	while($zaznam=$vysledek->fetch_assoc()) //pro kaûd˝ z·znam
    	{               
			echo '<tr><td>'.$zaznam["datum"].'</td><td>'.htmlspecialchars($zaznam["nadpis"],ENT_QUOTES).'</td><td><form method="post" action="action.php?type=edit"><input type="hidden" name="id" value="'.$zaznam["id"].'"><button type="submit">Upravit</button></form></td><td><form method="post" action="action.php?type=delete"><input type="hidden" name="id" value="'.$zaznam["id"].'"><button  type="submit">Smazat</button></form></td><td><form method="post" action="action.php?type=upload"><input type="hidden" name="id" value="'.$zaznam["id"].'"><input type="hidden" name="nadpis" value="'.$zaznam["nadpis"].'"><button  type="submit">Nahrát fotky</button></form></td></tr>';
  		}
 		echo '</table>';
 	}
}

function newAction ($nadpis, $obsah,$datum) 
{
            $nadpis=escape($nadpis);
            $obsah=escape($obsah);
        	$datum = date('Y-m-d', strtotime($datum));
            $dotaz="INSERT INTO akce (`nadpis`, `obsah`,`datum`) VALUES ('".$nadpis."', '".$obsah."', '".$datum."')";
            $vysledek=db_conn($dotaz);
            
            if($vysledek) 
            {
                echo "Akce vytvořena";
            }
            else 
            {
                echo "Nastala chyba při vytváření akce. Zkuste to prosím znovu.";
            }
}
        
function updateAction ($id,$nadpis,$obsah,$datum)
{
	$nadpis=escape($nadpis);
	$obsah=escape($obsah);
	$datum = date('Y-m-d', strtotime($datum));
	$dotaz="UPDATE akce SET  `nadpis` =  '".$nadpis."' , `obsah` =  '".$obsah."', `datum` =  '".$datum."' WHERE  `id` =  '".$id."';";   //zmÏna dat str·nky
	$vysledek=db_conn($dotaz);
            
            if($vysledek) 
            {
                echo "Změny uloženy!"; 
            }
            else 
            {
                echo "Nastala chyba při změně akce. Zkuste to prosím znovu.";
            }
}

function deleteDir($dirPath) 
{
    if (! is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            self::deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}

function deleteAction($id)
{
	$dotaz2="DELETE FROM akce WHERE id=".$id.";";  //posunutÌ vöech id nad mazan˝m id o -1  
	$vysledek2=db_conn($dotaz2);  //provedenÌ dotazu
 	if ($vysledek2) 
 	{
 		if (file_exists("actionimg/".$id."/"))
 		{
 		deleteDir("actionimg/".$id."/");
 		echo "Akce smazána!";
 		}
 		else
 		{
 		echo "Akce smazána!";
 		}
 				
 	}
	else 
 	{
		echo "Nastala chyba! Zkuste to prosím znovu";
  				
 	}
 		

} 




?>

<!DOCTYPE html>

<html>
    <head>
        <title>Harmonizační Studio Osm</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <div id="wrapper">
      <section>
   <?php   
      if (!isset($_GET["type"]))
      {
      	listAction();
      	echo '<br><form type="get"><input type="hidden" name="type" value="new"><button type="submit">Nová akce</button></form>';
      }
      else
      {
     	 if ($_GET["type"]=='new')
      	{
           echo '<form method="post" action="action.php?type=new">
                <input type="text" name="nadpis" placeholder="Nadpis"><br><br>
                <textarea type="text" name="obsah" placeholder="Obsah"></textarea><br><br>
                <input type="date" name="datum"></input><br><br>
                <button type="submit" name="ulozit">Uložit</button>
            	</form>';
            
            
            if(isset($_POST["ulozit"]))
            {
            	$obsah = '<pre>'.$_POST["obsah"].'</pre>';
            	newAction($_POST["nadpis"],$obsah,$_POST["datum"]);
            	$url="http://www.studioosm.cz/admin/action.php";
				echo ('<meta http-equiv="refresh" content="1;url='.$url.'">');
            }
      }
      	if ($_GET["type"]=='edit'&&isset($_POST["id"]))
      	{
      	$data=getAction($_POST["id"]);
      	echo '<form method="post" action="action.php?type=edit">
                <input type="hidden" name="id" value="'.$_POST["id"].'">
                <input type="text" name="nadpis" value="'.$data["nadpis"].'"><br><br>
                <textarea type="text" name="obsah">'.$data["obsah"].'</textarea><br><br>
                <input type="date" name="datum" value="'.$data["datum"].'"></input><br><br>
                <button type="submit" name="ulozit">Uložit</button>
            </form>';
      	if (isset($_POST["ulozit"]))
      	{
      		updateAction($_POST["id"],$_POST["nadpis"],$_POST["obsah"],$_POST["datum"]);
      		$url="http://www.studioosm.cz/admin/action.php";
			echo ('<meta http-equiv="refresh" content="1;url='.$url.'">');
      	}
      }
      	if ($_GET["type"]=='delete'&&isset($_POST["id"]))
      	{
      		echo 'Opravdu chcete smazat zvolenou akci? <br> <form method="post"><input type="hidden" name="id" value="'.$_POST["id"].'"><button type="submit" name="ano">ANO</button><button type="submit" name="ne">NE</button><br>';
			if (isset($_POST["ano"])) 
			{ 
				deleteAction($_POST["id"]); 
				$url="http://www.studioosm.cz/admin/action.php";
				echo ('<meta http-equiv="refresh" content="1;url='.$url.'">');
			
			}
			if (isset($_POST["ne"]))
			{	 
				header ('Refresh: 1;');
				$url="http://www.studioosm.cz/admin/action.php";
				echo ('<meta http-equiv="refresh" content="1;url='.$url.'">');
			}
      }
      	if ($_GET["type"]=='upload'&&isset($_POST["id"])||$_GET["type"]=='upload'&&isset($_POST["filetodelete"]))
      	{
      		include ('upload.php');
      	}
      	
      }
          ?>
          <h2><a href="admin.php">Zpět</a></h2>
            </section>
        </div>
    </body>
</html>




