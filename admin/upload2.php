
<?php
function listDirectory ($path, $mode)
{
	if (is_dir($path)) 
	{
		$allfiles = scandir($path);
		switch($mode) 
		{
      		case "view":
			$excludedfiles = array('..', '.');
			$files = array_diff($allfiles,$excludedfiles);
			echo 'Directory: '.$path.'<br>';
			echo '<table align="center" class="directorylist"><th>Name</th>';
			foreach ($files as $value) 
   			{ 
      			if (!is_dir($path.$value))
      			{
      			$pathparts = pathinfo($value);
      			$extension = $pathparts['extension'];
      			$name = $pathparts['filename'];
      			echo '<tr><td><a href="'.$path.$value.'"><img src="'.$path.$value.'">'.$name.'</a></td></tr>';
      			}
   			} 
   			echo '</table>';
        	break;
      	
      		case "edit":
      		$excludedfiles = array('.','..');
			$files = array_diff($allfiles,$excludedfiles);
      		echo '<table align="center" class="directorylist"><th>Obrázek</th>';
			foreach ($files as $value) 
   			{ 
      			$pathparts = pathinfo($value);
      			$extension = $pathparts['extension'];
      			$name = $pathparts['filename'];
      			
      			echo '<tr><td><a href="'.$path.$value.'"><img style="width: 100%"src="'.$path.$value.'"></a></td>
      				<td><form method="post" action="actual.php?type=upload"><input type="hidden" name="filetodelete" value="'.$path.$value.'"><button type="submit">Smazat</button></form></td></tr>';
   			} 
   			echo '</table>';
      		break;
      	//...
      			
      		default:
        	echo "Invalid mode!";
        	break;	
   		}
	}

}
function uploadFile ($targetDir, $extensions, $mode)
{
	
	if ($mode == 'multiple')
	{
		for ($j=0; $j < count($_FILES["fileToUpload"]['name']); $j++)
		{
			$file_name = basename($_FILES["fileToUpload"]['name']["$j"]);
			$target_file_path = $targetDir . $file_name;
			$uploadOk = 1;
			$extensionOk = 1;
			$fileExtension = pathinfo($file_name,PATHINFO_EXTENSION);
			// Check if file already exists
			if (file_exists($target_file_path)) 
			{
    			echo "Sorry, file already exists.";
    			$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]['size']["$j"] > 2000000) 
			{
    			echo "Sorry, your file is too large.";
    			$uploadOk = 0;
			}
			// Allow certain file formats
			if($extensions[0]!=='all')
			{
				foreach ($extensions as &$hodnota) 
				{
    				if ($hodnota == $fileExtension)
    				{
    					$uploadOk = 1;
    					$extensionOk = 1;
    					break;
    				}   
    				else
    				{
    					$extensionOk = 0;
    					$uploadOk=0;
    				} 
				}
			}
			else
			{
				if($fileExtension=='php')
				{
					$extensionOk = 0;
    					$uploadOk=0;
				}
			}
			if($uploadOk == 0 && $extensionOk == 0)
			{
				echo "Sorry, files with this extension are not allowed!";
			}
			if ($uploadOk == 0) // Check if $uploadOk is set to 0 by an error
			{
    			echo "Sorry, your file was not uploaded.";
			} 
			else // if everything is ok, try to upload file
			{
    			if (move_uploaded_file($_FILES["fileToUpload"]['tmp_name']["$j"], $target_file_path)) 
    			{
        			echo "The file ". basename( $_FILES["fileToUpload"]['name']["$j"]). " has been uploaded.";
    			} 
    			else 
    			{
        			echo "Sorry, there was an error uploading your file.";
    			}
			}
		}
	}
	
	if ($mode == 'single')
	{
		$file_name = basename($_FILES["fileToUpload"]['name']);
		$target_file_path = $targetDir . $file_name;
		$uploadOk = 1;
		$extensionOk = 1;
		$fileExtension = pathinfo($file_name,PATHINFO_EXTENSION);
// Check if file already exists
		if (file_exists($target_file_path)) 
		{
    		echo "Sorry, file already exists.";
    		$uploadOk = 0;
		}
// Check file size
		if ($_FILES["fileToUpload"]['size'] > 50000000) 
		{
    		echo "Sorry, your file is too large.";
    		$uploadOk = 0;
		}
// Allow certain file formats
		foreach ($extensions as &$hodnota) 
		{
    		if ($hodnota == $fileExtension)
    		{
    			$uploadOk = 1;
    			$extensionOk = 1;
    			break;
    		}   
    		else
    		{
    			$extensionOk = 0;
    			$uploadOk=0;
    		} 
		}
		if($uploadOk == 0 && $extensionOk == 0)
		{
			echo "Sorry, files with this extension are not allowed!";
		}
		if ($uploadOk == 0) // Check if $uploadOk is set to 0 by an error
		{
    		echo "Sorry, your file was not uploaded.";
		} 
		else // if everything is ok, try to upload file
		{
    		if (move_uploaded_file($_FILES["fileToUpload"]['tmp_name'], $target_file_path)) 
    		{
        		echo "The file ". $file_name. " has been uploaded.";
    		}	 
    		else 
    		{
        		echo "Sorry, there was an error uploading your file.";
    		}
		}
	}
	
}

function deleteFile ($path)
{
	if (file_exists($path)) 
	{
        unlink($path);
        echo 'Soubor byl vymazán!';
    }
    else
    {
    	echo 'Soubor neexistuje!';
    }
        
}

if (isset($_POST["id"]))
{
		echo '<h1>Nahrát fotky pro aktualitu: '.$_POST["nadpis"].'</h1>';
		echo '<form method="post" enctype="multipart/form-data">';
		echo 'Vyberte obrázky pro nahrání:<br><input type="hidden" name="id" value="'.$_POST["id"].'">
		<input type="file" name="fileToUpload[]" multiple="multiple" id="fileToUpload"><br><br>
		<button type="submit" name="upload">Nahrát soubory</button>
		</form><br><hr><br>';

		listDirectory("actualimg/".$_POST["id"]."/","edit");


		if(isset($_POST["upload"])) 
		{
			
				if (file_exists("actualimg/".$_POST["id"]))
				{
					uploadFile("actualimg/".$_POST["id"]."/",array("all"), 'multiple');
					$url="http://www.studioosm.cz/admin/actual.php";
				echo ('<meta http-equiv="refresh" content="0;url='.$url.'">');
				}
				else
				{
					mkdir("actualimg/".$_POST["id"], 0755);
					uploadFile("actualimg/".$_POST["id"]."/",array("all"), 'multiple');
					$url="http://www.studioosm.cz/admin/actual.php";
				echo ('<meta http-equiv="refresh" content="0;url='.$url.'">');
				}
			
		}

}
if(isset($_POST["filetodelete"]))
{
	echo 'Opravdu chcete vymazat soubor '.$_POST["filetodelete"].'? <br> <form method="post"><input type="hidden" name="filetodelete" value="'.$_POST["filetodelete"].'"><button type="submit" name="ano">ANO</button><button type="submit" name="ne">NE</button><br>';
	if (isset($_POST["ano"])) 
	{ 
		deleteFile($_POST["filetodelete"]); 
		$url="http://www.studioosm.cz/admin/actual.php";
				echo ('<meta http-equiv="refresh" content="1;url='.$url.'">');
	}
	if (isset($_POST["ne"]))
	{ 
		$url="http://www.studioosm.cz/admin/actual.php";
				echo ('<meta http-equiv="refresh" content="1;url='.$url.'">');
	}       
}
?>
