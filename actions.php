<?php
include ('admin/access.php');

function listActions()
{
	$dotaz="SELECT  * FROM  akce ORDER BY `datum` DESC; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
	$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 	if ($vysledek) 
 	{
    	
    	while($zaznam=$vysledek->fetch_assoc()) //pro kaûd˝ z·znam
    	{               
			echo '<div class="sluzby">
                <a href="action.php?id='.$zaznam["id"].'" title="'.$zaznam["nadpis"].'">'.$zaznam["nadpis"].'</a>
                </div>'; 		
        }
 		
 	}
}
?>
<!DOCTYPE html>

<html>
    <head>
        <title>Akce > Studio 8</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Akce Harmonizačního studia Osm">
        <meta name="author" content="Saara Sou">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    </head>
    <body>
        <div id="wrapper">
            <header>
                <a class="logo" id="up" href="http://www.studioosm.cz/uvod" title="logo"><img src="images/logo.png" alt="logo"></a>
                <nav>
                        <a href="http://www.studioosm.cz/aktuality" title="Aktuality" class="infinityleft">Aktuality</a>
                        <a class="infinityright" href="http://www.studioosm.cz/sluzby" title="Služby">Služby</a>
                    
                    
                        <a href="http://www.studioosm.cz/rozvrh" title="Rozvrh" class="infinityleft">Rozvrh</a>
                        <a class="infinityright" href="http://www.studioosm.cz/akce" title="Akce">Akce</a>
                    
                    
                        <a href="http://www.studioosm.cz/galerie" title="Galerie" class="infinityleft">Galerie</a>
                        <a class="infinityright" href="http://www.studioosm.cz/kontakt" title="Kontakt">Kontakt</a>
                </nav>
            </header>
            
            <section>
                <h1 class="pakcenadpis">proběhlé akce</h1>
                <p class="akce">
                    Pro nadcházející akce přejděte na stránku <a class="odkazaktuality" href="actual.html">aktuality</a>.
                </p>
                <?php
                listActions();
                ?>
                
            </section>
        </div>
    </body>
</html>