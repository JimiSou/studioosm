$(document).ready(function(){
      $('.single-item').slick({autoplay: true,
  autoplaySpeed: 4000, fade: true});
    
    $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav',
  swipeToSlide: true,
  adaptiveHeight: true
});
$('.slider-nav').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  centerMode: true,
  focusOnSelect: true,
  swipeToSlide: true,
  variableWidth: true
});

});