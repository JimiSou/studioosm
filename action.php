<?php
include ('admin/access.php');

function is_dir_empty($dir) {
  if (!is_readable($dir)) return NULL; 
  $handle = opendir($dir);
  while (false !== ($entry = readdir($handle))) {
    if ($entry != "." && $entry != "..") {
      return FALSE;
    }
  }
  return TRUE;
}


function getGallery ($id)
{
		$path='admin/actionimg/'.$id.'/';
		if (is_dir_empty('admin/actionimg/'.$id)||file_exists('admin/actionimg/'.$id))
		{
			$allfiles = scandir($path);
			$excludedfiles = array('..', '.');
			$files = array_diff($allfiles,$excludedfiles);
			echo '<div class="slider-for galeriebig">';
			
			foreach ($files as $value) 
   			{ 
   				
      			if (!is_dir($path.$value))
      			{
      				$pathparts = pathinfo($value);
      				$extension = $pathparts['extension'];
      				$name = $pathparts['filename'];
      				echo '<div><img src="'.$path.$value.'"></div>';
      			}
                        
   			} 
   			
   			echo '</div>';
   			echo '<div class="slider-nav galeriesmall">';
			
			foreach ($files as $value) 
   			{ 
   				
      			if (!is_dir($path.$value))
      			{
      				$pathparts = pathinfo($value);
      				$extension = $pathparts['extension'];
      				$name = $pathparts['filename'];
      				echo '<div><img src="'.$path.$value.'"></div>';
      			}
                        
   			} 
   			echo '</div>';
      	
        }        
      			
      		

}

function getAction($id)
{
	$dotaz="SELECT  * FROM  akce WHERE `id`='".$id."'; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
	$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 	
 		if ($vysledek)
			{
				
				return $vysledek->fetch_assoc(); //vr·cenÌ pole(celÈho z·znamu)
				
			}
			else 
			{
				echo "Nastala chyba! Zkuste to prosím znovu.";
			}
 	
}
$data=getAction($_GET["id"]);
?>

<!DOCTYPE html>

<html>
    <head>
        <title>Akce > Studio 8</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Harmonizační studio Osm v Srbské 2 nedaleko metra Hradčanská.
                                          Dvě místnosti na terapie a poradenství, třetí místnost slouží ke cvičení jógy, meditacím, k tanci a přednáškám či seminářům.

                                          Pojďte tedy nahlédnout do Harmonizačního studia Osm, jste srdečně vítáni!">
        <meta name="author" content="Saara Sou">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/slick.css"/>
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    </head>
    <body>
        <div id="wrapper">
            <header>
                <a class="logo" id="up" href="http://www.studioosm.cz/uvod" title="logo"><img src="images/logo.png" alt="logo"></a>
                <nav>
                        <a href="http://www.studioosm.cz/aktuality" title="Aktuality" class="infinityleft">Aktuality</a>
                        <a class="infinityright" href="http://www.studioosm.cz/sluzby" title="Služby">Služby</a>
                    
                    
                        <a href="http://www.studioosm.cz/rozvrh" title="Rozvrh" class="infinityleft">Rozvrh</a>
                        <a class="infinityright" href="http://www.studioosm.cz/akce" title="Akce">Akce</a>
                    
                    
                        <a href="http://www.studioosm.cz/galerie" title="Galerie" class="infinityleft">Galerie</a>
                        <a class="infinityright" href="http://www.studioosm.cz/kontakt" title="Kontakt">Kontakt</a>
                </nav>
            </header>
            
            <section>
                <div class="action">
                    <?php
                    echo '<h1>'.$data["nadpis"].'</h1>';
                    echo '<h2>'.$data["obsah"].'</h2>';
                    getGallery($_GET["id"]);
                    ?>
                </div>
            </section>
        </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="slick.min.js"></script>
        <script type="text/javascript" src="slick.js"></script>
    </body>
</html>