<?php
include ('admin/access.php');

function is_dir_empty($dir) {
  if (!is_readable($dir)) return NULL;
  $handle = opendir($dir);
  while (false !== ($entry = readdir($handle))) {
    if ($entry != "." && $entry != "..") {
      return FALSE;
    }
  }
  return TRUE;
}


function getGallery ($id)
{
		$path='admin/actualimg/'.$id.'/';
		if (is_dir_empty('admin/actualimg/'.$id)||file_exists('admin/actualimg/'.$id))
		{
			$allfiles = scandir($path);
			$excludedfiles = array('..', '.');
			$files = array_diff($allfiles,$excludedfiles);
			echo '<div class="single-item galeriebig">';

			foreach ($files as $value)
   			{

      			if (!is_dir($path.$value))
      			{
      				$pathparts = pathinfo($value);
      				$extension = $pathparts['extension'];
      				$name = $pathparts['filename'];
      				echo '<div><img src="'.$path.$value.'"></div>';
      			}

   			}



   			echo '</div>';

        }



}

function listActual()
{
	$dotaz="SELECT  * FROM aktuality ORDER BY `datum` DESC; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
	$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 	if ($vysledek)
 	{

    	while($zaznam=$vysledek->fetch_assoc()) //pro kaûd˝ z·znam
    	{
			echo '<div class="aktuality">
                    <h2>'.$zaznam["nadpis"].'</h2>
                    <h3>
                        '.$zaznam["obsah"].'
                    </h3>';
            echo getGallery($zaznam["id"]);
        	echo '</div>';
  		}

 	}
}
?>

<!DOCTYPE html>

<html>
    <head>
        <title>Aktuality > Studio 8</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Aktuality Harmonizačního studia Osm">
        <meta name="author" content="Saara Sou">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    </head>
    <body>
        <div id="wrapper">
            <header>
                <a class="logo" id="up" href="http://www.studioosm.cz/uvod" title="logo"><img src="images/logo.png" alt="logo"></a>
                <nav>
                        <a href="http://www.studioosm.cz/aktuality" title="Aktuality" class="infinityleft">Aktuality</a>
                        <a class="infinityright" href="http://www.studioosm.cz/sluzby" title="Služby">Služby</a>


                        <a href="http://www.studioosm.cz/rozvrh" title="Rozvrh" class="infinityleft">Rozvrh</a>
                        <a class="infinityright" href="http://www.studioosm.cz/akce" title="Akce">Akce</a>


                        <a href="http://www.studioosm.cz/galerie" title="Galerie" class="infinityleft">Galerie</a>
                        <a class="infinityright" href="http://www.studioosm.cz/kontakt" title="Kontakt">Kontakt</a>
                </nav>
            </header>

            <section>
                <h1 class="aktualitynadpis">Aktuality</h1>

                <div class="aktualityuvod">
                <img class="aktualitykvet" src="images/kvet.png" alt="kvet">
                <img class="aktualitykvet1" src="images/kvet.png" alt="kvet">
                <h2>Nabídka pronájmu terapeutické místnosti, přednáškové místnosti<br/>nebo uspořádání Vašeho semináře</h2>
                <p class="white">Prosím kontaktujte vedoucí studia <br/>
                    Lenku Kubíkovou na mail: aromalenka@volny.cz nebo na tel.: 606 702 546</p>
                </div>
                <?php listActual(); ?>
            </section>
        </div>
    </body>
</html>
